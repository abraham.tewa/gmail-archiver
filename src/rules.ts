// ============================================================
// Import modules
import { ActionTypes, ArchiveRules } from './enums';
import { createSearchFilter } from './utils';

import type {
  Rule,
} from './types';

// ============================================================
// Functions
function archiveRule() : Rule {
  let limitDate: Date;

  const searches : string[] = [];
  const now = new Date();

  // Important read
  limitDate = new Date(now);
  limitDate.setDate(limitDate.getDate() - ArchiveRules.important_read);
  searches.push(createSearchFilter({
    labels: ['inbox'], important: true, read: true, before: limitDate,
  }));

  // Important unread
  limitDate = new Date(now);
  limitDate.setDate(limitDate.getDate() - ArchiveRules.important_unread);
  searches.push(createSearchFilter({
    labels: ['inbox'], important: true, read: false, before: limitDate,
  }));

  // Not important read
  limitDate = new Date(now);
  limitDate.setDate(limitDate.getDate() - ArchiveRules.not_important_read);
  searches.push(createSearchFilter({
    labels: ['inbox'], important: false, read: true, before: limitDate,
  }));

  // Not important unread
  limitDate = new Date(now);
  limitDate.setDate(limitDate.getDate() - ArchiveRules.not_important_unread);
  searches.push(createSearchFilter({
    labels: ['inbox'], important: false, read: false, before: limitDate,
  }));

  const rule: Rule = {
    searches,
    actions: [{
      type: ActionTypes.MOVE_TO_ARCHIVE,
    }],
  };

  return rule;
}

function getRules() {
  return [
    archiveRule(),
  ];
}

// ============================================================
// Exports
export {
  getRules,
};
