import type { ActionTypes } from './enums';

type Action = ApplyLabelAction | RemoveLabelAction | {
  type: Exclude<ActionTypes, ActionTypes.APPLY_LABEL | ActionTypes.REMOVE_LABEL>
};

type ApplyLabelAction = {
  type: ActionTypes.APPLY_LABEL,
  labels: string[],
};

type RemoveLabelAction = {
  type: ActionTypes.REMOVE_LABEL,
  labels: string[],
};

type Rule = {
  actions: Action[]
  searches: string[]
};

export type {
  Action,
  Rule,
};
