// ============================================================
// Import modules
import applyRules from './applyRules';
import Context from './Context';
import { getRules } from './rules';

// ============================================================
// Functions
// eslint-disable-next-line @typescript-eslint/no-unused-vars
function install() {
  // Clearing triggers
  console.log('bonjour 2');
  const triggers = ScriptApp.getProjectTriggers();

  triggers.forEach((trigger) => {
    ScriptApp.deleteTrigger(trigger);
  });

  // Adding trigger
  const trigger = ScriptApp.newTrigger('start');
  trigger.timeBased().everyMinutes(30).create();
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function start() {
  const context = new Context();
  const rules = getRules();

  applyRules(context, rules);
}
