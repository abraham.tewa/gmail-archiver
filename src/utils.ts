function createSearchFilter(
  options : {
    after?: Date | number,
    before?: Date | number,
    important?: boolean,
    labels?: string[],
    read?: boolean,
  },
) {
  let search = '';

  // After
  if (options.after instanceof Date) {
    search += ` after:${options.after.getTime() / 1000}`;
  } else if (options.after) {
    search += ` after:${options.after}`;
  }

  // Before
  if (options.before instanceof Date) {
    search += ` before:${options.before.getTime() / 1000}`;
  } else if (options.before) {
    search += ` before:${options.before}`;
  }

  // Important
  if (typeof options.important === 'boolean') {
    search += ` ${options.important ? '' : '-'}is:important`;
  }

  // Label
  if (options.labels) {
    search += options.labels.reduce((label, acc) => `${acc} label:${label}`, '');
  }

  // Read
  if (typeof options.read === 'boolean') {
    search += ` is:${options.read ? 'read' : 'unread'}`;
  }

  return search;
}

export {
  createSearchFilter,
};
