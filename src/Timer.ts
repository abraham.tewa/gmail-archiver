class Timer {
  private endTime: Date;

  private startTime: Date;

  constructor() {
    this.startTime = new Date();
    this.endTime = new Date(this.startTime);

    this.endTime.setMinutes(this.startTime.getMinutes() + 5);
    this.endTime.setSeconds(this.startTime.getSeconds() + 30);
  }

  canContinue(): boolean {
    return new Date() < this.endTime;
  }
}

export default Timer;
