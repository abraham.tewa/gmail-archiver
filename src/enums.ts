enum ActionTypes {
  APPLY_LABEL = 'Apply label',
  CLEAR_LABEL = 'Clear all labels',
  MOVE_TO_ARCHIVE = 'Move to archive',
  MOVE_TO_INBOX = 'Move to inbox',
  MOVE_TO_TRASH = 'Move to trash',
  MOVE_TO_SPAM = 'Move to spam',
  MARK_AS_READ = 'Mark as read',
  MARK_AS_UNREAD = 'Mark as unread',
  MARK_AS_IMPORTANT = 'Mark as important',
  MARK_AS_UNIMPORTANT = 'Mark as unimportant',
  REMOVE_LABEL = 'Remove label',
  STAR_MESSAGES = 'Star messages',
  UNSTAR_MESSAGES = 'Unstar messages',
}

enum ArchiveRules {
  important_unread = -14,
  important_read = -7,
  not_important_unread = -7,
  not_important_read = 3,
}

export {
  ActionTypes,
  ArchiveRules,
};
