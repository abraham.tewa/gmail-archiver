import { ActionTypes } from './enums';

import type Context from './Context';
import type { Action, Rule } from './types';

// ============================================================
// Functions
function applyRules(
  context: Context,
  rules: Rule[],
) {
  for (const rule of rules) {
    for (const search of rule.searches) {
      if (!context.canContinue()) { return; }
      const threads = GmailApp.search(search);

      for (const thread of threads) {
        for (const action of rule.actions) {
          if (!context.canContinue()) { return; }
          applyRuleToThread(context, thread, action);
        }
      }
    }
  }
}

function applyRuleToThread(
  context: Context,
  thread: GoogleAppsScript.Gmail.GmailThread,
  action: Action,
) {
  switch (action.type) {
    case ActionTypes.APPLY_LABEL:
      action.labels.forEach((label) => {
        if (!context.canContinue()) { return; }

        const gmailLabel = context.getOrCreateLabel(label);
        thread.addLabel(gmailLabel);
      });

      break;

    case ActionTypes.MARK_AS_IMPORTANT:
      thread.markImportant();
      break;

    case ActionTypes.MARK_AS_READ:
      thread.markRead();
      break;

    case ActionTypes.MARK_AS_UNIMPORTANT:
      thread.markUnimportant();
      break;

    case ActionTypes.MARK_AS_UNREAD:
      thread.markUnread();
      break;

    case ActionTypes.MOVE_TO_ARCHIVE:
      thread.moveToArchive();
      break;

    case ActionTypes.MOVE_TO_INBOX:
      thread.moveToInbox();
      break;

    case ActionTypes.MOVE_TO_SPAM:
      thread.moveToSpam();
      break;

    case ActionTypes.MOVE_TO_TRASH:
      thread.moveToTrash();
      break;

    case ActionTypes.REMOVE_LABEL:
      action.labels.forEach((label) => {
        if (!context.canContinue()) { return; }

        const gmailLabel = context.getLabel(label);

        if (!gmailLabel) {
          return;
        }

        thread.removeLabel(gmailLabel);
      });

      break;

    case ActionTypes.STAR_MESSAGES: {
      const messages = thread.getMessages();

      messages.forEach((message) => {
        if (!context.canContinue()) { return; }

        message.star();
      });

      break;
    }

    case ActionTypes.UNSTAR_MESSAGES: {
      const messages = thread.getMessages();

      messages.forEach((message) => {
        if (!context.canContinue()) {
          return;
        }

        message.star();
      });

      break;
    }
    default:
  }
}

export default applyRules;
