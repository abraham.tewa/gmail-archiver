import Timer from './Timer';

class Context {
  private labels: Record<string, GoogleAppsScript.Gmail.GmailLabel> = {};

  private timer = new Timer();

  canContinue() {
    return this.timer.canContinue();
  }

  getLabel(label: string): GoogleAppsScript.Gmail.GmailLabel {
    if (this.labels[label]) {
      return this.labels[label];
    }

    this.labels[label] = GmailApp.getUserLabelByName(label);

    return this.labels[label];
  }

  getOrCreateLabel(label: string): GoogleAppsScript.Gmail.GmailLabel {
    let gmailLabel: GoogleAppsScript.Gmail.GmailLabel;

    if (this.labels[label]) {
      return this.labels[label];
    }

    gmailLabel = GmailApp.getUserLabelByName(label);

    if (gmailLabel === null) {
      gmailLabel = GmailApp.createLabel(label);
      this.labels[label] = gmailLabel;
    }

    return gmailLabel;
  }
}

export default Context;
